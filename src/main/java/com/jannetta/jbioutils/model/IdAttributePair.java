package com.jannetta.jbioutils.model;

public class IdAttributePair {

	String id;
	String attribute;
	
	IdAttributePair(String id, String attribute) {
		this.id = id;
		this.attribute = attribute;
	}
}
