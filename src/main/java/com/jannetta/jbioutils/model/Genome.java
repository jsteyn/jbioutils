package com.jannetta.jbioutils.model;

public class Genome {
	private int A = 0, T = 0, G = 0, C = 0, N = 0;

	public Genome() {

	}

	public int getT() {
		return T;
	}

	public void setT(int t) {
		T = t;
	}

	public int getG() {
		return G;
	}

	public void setG(int g) {
		G = g;
	}

	public int getC() {
		return C;
	}

	public void setC(int c) {
		C = c;
	}

	public int getN() {
		return N;
	}

	public void setN(int n) {
		N = n;
	}

	public int getA() {
		return A;
	}

	public void setA(int a) {
		A = a;
	}

	public void incA() {
		A++;
	}

	public void incT() {
		T++;
	}

	public void incG() {
		G++;
	}

	public void incC() {
		C++;
	}

	public void incN() {
		N++;
	}
	
	public double percA() {
		return A/(double)total();
	}
	
	public double percT() {
		return T/(double)total();
	}
	
	public double percG() {
		return G/(double)total();
	}

	public double percC() {
		return C/(double)total();
	}
	
	public double percN() {
		return N/(double)total();
	}
	
	public int total() {
		return getA()+getT()+getG()+getC()+getN();
	}

}
