package com.jannetta.jbioutils.model;

public class ExonPair implements Comparable<ExonPair> {
	private final int start;
	private final int end;

	public ExonPair(int start, int end) {
		// if (end < start) throw new IllegalArgumentException("Invalid gene
		// specified");
		if (end < start) {
			this.end = start;
			this.start = end;
		} else {
			this.start = start;
			this.end = end;
		}
	}

	public int length() {
		return end - start + 1;
	}

	public int getStart() {
		return start;
	}

	public int getEnd() {
		return end;
	}

	public boolean overlaps(ExonPair newPair) {
		return !(newPair.start > end || newPair.end < start);
	}

	public boolean isAdjacent(ExonPair newPair) {
		return end == newPair.start - 1 || start == newPair.end + 1;
	}

	public boolean canMerge(ExonPair newPair) {
		return overlaps(newPair) || isAdjacent(newPair);
	}

	public boolean isBefore(ExonPair newPair) {
		return end < newPair.start;
	}

	public boolean isAfter(ExonPair newPair) {
		return start > newPair.end;
	}

	public ExonPair merge(ExonPair newPair) {
		if (!canMerge(newPair))
			throw new IllegalArgumentException(
					"Cannot merge these two genes, they do not overlap or are adjacent in proximity");
		int newStart = start <= newPair.start ? start : newPair.start;
		int newEnd = end >= newPair.end ? end : newPair.end;
		return new ExonPair(newStart, newEnd);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("[");
		sb.append(start);
		sb.append(", ").append(end);
		sb.append(']');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof ExonPair))
			return false;

		ExonPair genePair = (ExonPair) o;

		if (getStart() != genePair.getStart())
			return false;
		return getEnd() == genePair.getEnd();
	}

	@Override
	public int hashCode() {
		int result = getStart();
		result = 31 * result + getEnd();
		return result;
	}

	@Override
	public int compareTo(ExonPair o) {
		return end < o.start ? -1 : end > o.start ? 1 : 0;
	}
}
