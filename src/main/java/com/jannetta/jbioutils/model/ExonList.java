package com.jannetta.jbioutils.model;


import java.util.Collections;
import java.util.LinkedList;

public class ExonList {
    private LinkedList<ExonPair> gene = new LinkedList<>();

    public ExonList() {
    }

    public void addGenePair(ExonPair newPair) {
        LinkedList<ExonPair> needMerge = new LinkedList<>();

        ExonPair merged = newPair;
        for (final ExonPair g : gene) {
            if (merged.canMerge(g)) {
                needMerge.add(g);
                merged = merged.merge(g);
            }
        }

        gene.removeAll(needMerge);
        gene.add(merged);

        Collections.sort(gene);
    }

    public int count() {
        int sum = 0;
        for (final ExonPair g : gene) {
            sum += g.length();
        }
        return sum;
    }

    @Override
    public String toString() {
        return gene.toString();
    }
    
    public String sequenceToString() {
    	return "";
    }

}
