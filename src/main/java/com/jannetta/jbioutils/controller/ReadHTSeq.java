package com.jannetta.jbioutils.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

public class ReadHTSeq {
	private int matrix[][];
	private ArrayList<String> rownames = new ArrayList<String>();
	private ArrayList<String> samplenames = new ArrayList<String>();
	
	public ReadHTSeq(int[][]matrix, ArrayList<Double> genelengths) {
		
	}

	public ReadHTSeq(String filename) {
		// we can't initialise the matrix yet as we don't know how many
		// genes and how many samples there are
		int matrix[][]; 
		int samples = 0;
		int rows = -1;
		ArrayList<ArrayList<Integer>> allrows = new ArrayList<ArrayList<Integer>>();
		try {
			Scanner sc = new Scanner(new File(filename));
			String line = sc.nextLine();
			String[] tokens = line.split("\t");
			String[] samplenames = new String[tokens.length-1];
			// get the samplenames from the first line of the file
			// the first column should be empty or contain the heading
			// for the genenames
			for (int i = 0; i < samplenames.length; i++) {
				samplenames[i] = tokens[i+1];
			}
			this.samplenames = new ArrayList<String>(Arrays.asList(samplenames));
			// we now know how many samples there are
			samples = samplenames.length;
			// read the rest of the file
			// each line is a row and each column is a sample
			while (sc.hasNextLine()) {
				// an arraylist containing the counts for each sample
				ArrayList<Integer> row = new ArrayList<Integer>();
				// increment the row count for each line read
				rows++;
				line = sc.nextLine();
				tokens = line.split("\t");
				// this should be the genename
				rownames.add(tokens[0]);
				// these should be the counts per sample
				for (int i = 1; i < tokens.length; i++) {
					row.add(Integer.valueOf(tokens[i]));
				}
				allrows.add(row);
			}
			
			sc.close();
			// we now know how many genes there are so we can initialise
			// the matrix
			matrix = new int[rows + 1][samples];
			Iterator<ArrayList<Integer>> it_allrows = allrows.iterator();
			int mrow = -1;
			while (it_allrows.hasNext()) {
				mrow++;
				ArrayList<Integer> row = it_allrows.next();
				Iterator<Integer> it_samples = row.iterator();
				int mcol = -1;
				while (it_samples.hasNext()) {
					mcol++;
					matrix[mrow][mcol] = Integer.valueOf(it_samples.next());
				}
			}
			this.matrix = matrix;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public ReadHTSeq(String path, String[] filenames) {
		for (int file = 0; file < filenames.length; file++) {
			ArrayList<Integer> col = new ArrayList<Integer>();
			try {
				Scanner sc = new Scanner(new File(path + "/" + filenames[file]));
				// Read all the rows in the file
				while (sc.hasNextLine()) {
					String line = sc.nextLine();
					if (!line.startsWith("_")) {
						String[] tokens = line.split("\t");
						if (file == 0) {
							rownames.add(tokens[0]);
						}
						int rowval = Integer.valueOf(tokens[1]);
						col.add(rowval);
					}
				}
				sc.close();
				// If it is the first file in the list then initialise the
				// matrix
				if (file == 0) {
					matrix = new int[col.size()][filenames.length];
				}
				Iterator<Integer> i = col.iterator();
				// fill the column of the matrix
				int matrow = 0;
				while (i.hasNext()) {
					matrix[matrow][file] = i.next();
					matrow++;
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	public int[][] getMatrix() {
		return matrix;
	}
	
	public ArrayList<String> getSamplenames() {
		return samplenames;
	}

	public double[][] getNormMatrix() {
		double[][] norm = new double[matrix.length][matrix[0].length];
		double[][] tmp = new double[matrix.length][matrix[0].length];
		double[] geomeans = new double[matrix.length];
		// Get the geometric mean for each row of the matrix (i.e. for each
		// gene)
		for (int row = 0; row < geomeans.length; row++) {
			geomeans[row] = geoMean(matrix[row]);
			// Divide each value by the geomeans of its row
			for (int col = 0; col < matrix[0].length; col++) {
				tmp[row][col] = matrix[row][col] / geomeans[row];
			}
		}
		// The sizeFactor for each columns is the median of the column of tmp
		double sizeFactors[] = new double[matrix.length];
		for (int col = 0; col < tmp[0].length; col++) {
			int[] rows = new int[matrix.length];
			for (int row = 0; row < tmp.length; row++) {
				rows[row] = matrix[row][col];
			}
			sizeFactors[col] = median(rows);
			System.out.println(sizeFactors[col]);
		}
		// Now multiply each value by the sizeFactor of its column
		for (int col = 0; col < matrix[0].length; col++) {
			for (int row = 0; row < matrix.length; row++) {
				norm[row][col] = matrix[row][col] * sizeFactors[col];
			}

		}

		return norm;
	}

	public double mean(double[] col) {
		double mean = 0;
		for (int i = 0; i < col.length; i++) {
			mean += col[i];
		}
		return mean / (float) col.length;
	}

	public double median(int[] col) {
		Arrays.sort(col);
		double median;
		if (col.length % 2 == 0)
			median = ((double) col[col.length / 2] + (double) col[col.length / 2 - 1]) / 2;
		else
			median = (double) col[col.length / 2];
		return median;
	}

	public double geoMean(int[] row) {
		double[] ans = new double[row.length];
		for (int i = 0; i < ans.length; i++) {
			ans[i] = Math.log(row[i]);
		}
		return Math.exp((mean(ans)));
	}

	public ArrayList<String> getRownames() {
		return rownames;
	}

	public double[][] RPKM(int[][] matrix, ArrayList<Double> genelength) {
		double[][] rpkm = new double[matrix.length][matrix[0].length];
		double pmscalingfactor[] = new double[matrix[0].length];
		// Count up the counts per sample ...
		for (int sample = 0; sample < matrix[0].length; sample++) {
			double coltotal = 0;
			for (int row = 0; row < matrix.length; row++) {
				coltotal += matrix[row][sample];
			}
			// ... and divide by 1000000 for "per million scaling factor"
			pmscalingfactor[sample] = coltotal/1000000;
		}
		// divide read counts by per million scaling factor of sample
		// giving reads per million (RPM)
		for (int sample = 0; sample < matrix[0].length; sample++) {
			for (int row = 0; row < matrix.length; row++) {
				rpkm[row][sample] = matrix[row][sample]/pmscalingfactor[sample];
			}
		}
		// divide reads per million (RPM) by gene length giving RPKM
		for (int sample = 0; sample < matrix[0].length; sample++) {
			for (int row = 0; row < matrix.length; row++) {
				rpkm[row][sample] = rpkm[row][sample]/genelength.get(row);
			}
		}
		
		return rpkm;
	}

	public double[][] TPM(int[][] matrix, ArrayList<Double> genelength) {
		double[][] tpm = new double[matrix.length][matrix[0].length];
		double pmscalingfactor[] = new double[matrix[0].length];
		// divide read counts by the length of each gene (RPK)
		for (int sample = 0; sample < matrix[0].length; sample++) {
			for (int row = 0; row < matrix.length; row++) {
				tpm[row][sample] = matrix[row][sample]/genelength.get(row);
			}
		}
		// Count up the RPKs per sample ...
		for (int sample = 0; sample < matrix[0].length; sample++) {
			double coltotal = 0;
			for (int row = 0; row < matrix.length; row++) {
				coltotal += tpm[row][sample];
			}
			// ... and divide by 1000000 for "per million scaling factor"
			pmscalingfactor[sample] = coltotal/1000000;
		}
		// divide reads per kilobase (RPK) by gene length giving TPM
		for (int sample = 0; sample < matrix[0].length; sample++) {
			for (int row = 0; row < matrix.length; row++) {
				tpm[row][sample] = tpm[row][sample]/pmscalingfactor[sample];
			}
		}
		
		return tpm;
	}


}
