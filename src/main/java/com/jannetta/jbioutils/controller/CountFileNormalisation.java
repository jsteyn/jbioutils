import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class CountFileNormalisation {

	static public void main(String[] args) {
		File path = new File(args[0]);
		System.out.println("Path: " + args[0]);
		File[] filelist = path.listFiles();
		//File[] filelist = {new File("1.counts")};
		String[] filenames = new String[filelist.length];
		for (int i = 0; i < filelist.length; i++) {
			filenames[i] = filelist[i].getName();
		}
		ReadHTSeq rt = new ReadHTSeq(path.getAbsolutePath(), filenames);
		int matrix[][] = rt.getMatrix();
		double norm[][] = rt.getNormMatrix();
		ArrayList<String> rownames = rt.getRownames(); 
		System.out.println(matrix.length);
		System.out.println(matrix[0].length);
		try {
			PrintWriter counts = new PrintWriter(new File("counts.txt"));
			PrintWriter norms = new PrintWriter(new File("normcounts.txt"));
			counts.print("\t");
			norms.print("\t");
			for (int col = 0; col < matrix[0].length; col++) {
				counts.print(filenames[col] + "\t");
				norms.print(filenames[col] + "\t");
			}
			counts.println();
			norms.println();
			
			for (int row = 0; row < matrix.length; row++) {
				counts.print(rownames.get(row) + "\t");
				norms.print(rownames.get(row) + "\t");
				for (int col = 0; col < matrix[0].length; col++) {
					counts.print(matrix[row][col] + "\t");
					norms.print(norm[row][col] + "\t");
				}
				counts.println();
				norms.println();
			}
			counts.close();
			norms.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}