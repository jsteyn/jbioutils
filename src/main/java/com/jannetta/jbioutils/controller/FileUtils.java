package com.jannetta.jbioutils.controller;

import java.io.File;

public class FileUtils {

	public static File[] getFiles(String directory) {
		File[] files;
		files = new File(directory).listFiles();
		return files;
	}

}
