package com.jannetta.jbioutils.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class HtseqCountUtils {

	public static void createMatrixFile(File[] files, String outputfile) {
		Scanner[] sc = new Scanner[files.length];
		try {
			PrintWriter pw = new PrintWriter(new File(outputfile));
			pw.print("Gene");
			// open all files
			for (int i = 0; i < files.length; i++) {
				sc[i] = new Scanner(files[i]);
				pw.print("\t" + files[i].getName());
			}
			pw.println();

			// read one line per file and write to output
			while (sc[0].hasNextLine()) {
				String line = "";
				for (int i = 0; i < files.length; i++) {
					line = sc[i].nextLine();
					if (!line.startsWith("__")) {
						String[] tokens = line.split("\t");
						if (i == 0) {
							pw.print(tokens[0] + "\t" + tokens[1]);
						} else {
							pw.print("\t" + tokens[1]);
						}
					}
				}
				if (!line.startsWith("__")) {
					pw.println();
				}
			}

			// Close all
			for (int i = 0; i < files.length; i++) {
				sc[i].close();
			}
			pw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
