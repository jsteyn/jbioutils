package com.jannetta.jbioutils.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Hashtable;
import java.util.Scanner;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;

/**
 * @author Jannetta S Steyn
 * 
 *         This program will create a tab separated file consisting of the
 *         ENSEMBL id and the gene name. When exporting from Ensembl
 *         (http://www.ensembl.org/biomart/martview/), the version of the
 *         Ensembl id is in a separate column. This program merely takes this
 *         third column containing the version number and adds it to the Ensembl
 *         id with a dot (.) Select Gene ID and Version (gene) attributes or
 *         Select Transcript ID and Version (transcript)
 *
 */
public class EnsemblExportToENSG_Name_Version_conversion {

	static String inputfile;
	static String outfile;
	static Hashtable<String, String> id_att = new Hashtable<String, String>();

	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("Sytax: java -jar EnsemblExportToENSG.jar input_file output_file");
		} else {
			inputfile = args[0];
			outfile = args[1];
			try {
				Scanner sc_input = new Scanner(new File(inputfile));
				PrintWriter pw_outfile = new PrintWriter(new File(outfile));
				String[] header = sc_input.nextLine().split("\t");
				// Read id file into hashtable
				while (sc_input.hasNextLine()) {
					String line = sc_input.nextLine();
					String[] tokens = line.split("\t");
					if (header.length == 2) {
						if (tokens[1].equals("")) {
							pw_outfile.println(tokens[0] + "\t" + "NA");
						} else {
							pw_outfile.println(tokens[0] + "\t" + tokens[1]);
						}
					} else {
						if (tokens[1].equals("")) {
							pw_outfile.println(tokens[0] + "." + tokens[1] + "\t" + "NA");
						} else {
							pw_outfile.println(tokens[0] + "." + tokens[1] + "\t" + tokens[2]);
						}
					}
				}
				pw_outfile.close();
				sc_input.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Prints the help text explaining command line parameters
	 * 
	 * @param options
	 */
	private static void help(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(
				"java -cp jbioutils.jar com.jannetta.view.EnsemblExportToENSG_Name_Version_conversion\n" + "Version: 1\n"
						+ "Collating genome statistics",
				options);
	}
}
