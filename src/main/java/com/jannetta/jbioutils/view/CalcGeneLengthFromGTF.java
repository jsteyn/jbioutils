package com.jannetta.jbioutils.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;

import com.jannetta.jbioutils.model.ExonList;
import com.jannetta.jbioutils.model.ExonPair;

/**
 * @author Jannetta S Steyn
 * @author Su-Yang Yu
 * 
 *         The purpose of this program is to calculate the number of nucleotides
 *         that are within exons using a gtf file. Features in the gtf files
 *         must have a gene_id (which is used as the primary key), a
 *         transcript_type of protein_coding and it should be an exon.
 * 
 */
public class CalcGeneLengthFromGTF {
	static boolean log = false;
	static boolean ignoregenelist = true;
	static ArrayList<String> genelist = new ArrayList<String>();
	static String gtffile = "";
	static String genelistfile = "";
	static String outputfile = "";
	static boolean version = true;

	public static void main(String[] args) {
		Options options = new Options();
		CommandLineParser parser = new DefaultParser();

		Option gtf = Option.builder("i").hasArg().required().desc("gtf file filename").build();
		Option g = Option.builder("g").hasArg().required().desc("File containing all gene IDs.").build();
		Option o = Option.builder("o").hasArg().required().desc("Name of file to write gene length to.").build();
		Option nv = Option.builder("nv")
				.desc("If this switch is used then version numbers in the gene or transcript IDs the gtf file are truncated.")
				.build();
		options.addOption(gtf);
		options.addOption(g);
		options.addOption(o);
		options.addOption(nv);

		try {
			CommandLine cmd = parser.parse(options, args);
			if (cmd.hasOption("i")) {
				gtffile = cmd.getOptionValue("i");
			}
			if (cmd.hasOption("g")) {
				genelistfile = cmd.getOptionValue("g");
			}
			if (cmd.hasOption("o")) {
				outputfile = cmd.getOptionValue("o");
			}
			if (cmd.hasOption("nv")) {
				version = false;
			}
					
		} catch (MissingOptionException e) {
			help(options);
			System.exit(1);
		} catch (UnrecognizedOptionException e) {
			help(options);
			System.exit(1);
		} catch (MissingArgumentException e) {
			System.out.println("You need to provide a value for -" + e.getOption().getOpt());
			System.exit(1);
		} catch (ParseException e) {
			System.err.println("Parsing failed.  Reason: " + e.getMessage());
			System.exit(1);
		}
		try {
			ignoregenelist = false;
			Scanner sc_genelist = new Scanner(new File(genelistfile));
			System.out.println("Reading list of genes ...");
			while (sc_genelist.hasNext()) {
				String gene_id1 = sc_genelist.nextLine().trim();
				if (!version) {
					if (gene_id1.indexOf('.') > -1) 
					gene_id1 = gene_id1.substring(0, gene_id1.charAt('.'));
				}
				genelist.add(gene_id1);
			}
			sc_genelist.close();
			PrintWriter pw = new PrintWriter(new File(outputfile));
			int lineinserted = 1;
			System.out.println("Reading gtf file ...");
			Scanner sc = new Scanner(new File(gtffile)); // gtf_infile
			/**
			 * Hashtable containing gene_id, start position and end position The
			 * gene_id is used as the key. The value is of type model.Gene
			 */
			int linecount = 0;
			Hashtable<String, ExonList> exonsingene = new Hashtable<String, ExonList>();
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				if (line.startsWith("#")) {
					// skip line
				} else {
					String[] tokens = line.split("\t");
					// only continue if this is an exon
					if (tokens[2].equals("exon")) {
						String[] infoline = tokens[8].split("; ");
						String transcript_type = "";
						String gene_id = "";
						String gene_biotype = "";
						// parse info section of line read
						for (int i = 0; i < infoline.length; i++) {
							String[] in = infoline[i].split(" ");
							if (in[0].equals("transcript_type")) {
								transcript_type = in[1];
							}
							if (in[0].equals("gene_id")) {
								gene_id = trimquote(in[1].trim());
								// If not using versions then trim the version if it exists
								if (!version) {
									if (gene_id.indexOf('.') > -1) 
									gene_id = gene_id.substring(0, gene_id.indexOf('.'));
								}
							}
							if (in[0].equals("gene_biotype")) {
								gene_biotype = trimquote(in[1].trim());
							}
						}

						if (ignoregenelist || genelist.contains(gene_id)) {
							if (exonsingene.containsKey(gene_id)) {
								// if gene already in output list, add the
								// feature
								exonsingene.get(gene_id).addGenePair(
										new ExonPair(Integer.valueOf(tokens[3]), Integer.valueOf(tokens[4])));
							} else {
								// if gene not already in output list, then
								// add the gene and the feature
								ExonList newgene = new ExonList();
								newgene.addGenePair(
										new ExonPair(Integer.valueOf(tokens[3]), Integer.valueOf(tokens[4])));
								// add feature
								exonsingene.put(gene_id, newgene);
							}
							linecount++;
						}
					}
				}

			}
			// Read the hashtable of genes and print them to the
			// output file
			System.out.println("Counted genes: " + exonsingene.size());
			Enumeration<String> keys = exonsingene.keys();
			while (keys.hasMoreElements()) {
				String key = keys.nextElement();
				pw.println(key + "\t" + exonsingene.get(key).count());
			}
			sc.close();
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static String trimquote(String str) {
		while (str.charAt(0) == '"') {
			str = str.substring(1);
		}
		if (str.charAt(str.length() - 1) == '"') {
			str = str.substring(0, str.length() - 1);
		}
		return str;
	}

	/**
	 * Prints the help text explaining command line parameters
	 * 
	 * @param options
	 */
	private static void help(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("java -cp jbioutils.jar com.jannetta.view.CalcGeneLength\n" + "Version: 1\n"
				+ "Calculate gene length from gtf file for use in TPM calculations.", options);
	}
}
