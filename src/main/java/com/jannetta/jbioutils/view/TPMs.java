package com.jannetta.jbioutils.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.jannetta.jbioutils.controller.ReadHTSeq;

public class TPMs {
	static int[][] countmatrix;
	static String[] genenames;
	static ArrayList<Double> genelengths;
	static String[] samplenames;
	static String tpmfilename;
	static String argument = "TPM";
	static int number_of_samples;
	static int number_of_genes;
	static String gf;
	static String cf;

	private static void readMatrixFile() {
		countmatrix = new int[number_of_genes][number_of_samples];
		genenames = new String[number_of_genes];
		genelengths = new ArrayList<Double>();
		samplenames = new String[number_of_samples];

		try {
			// Scanner for file containing counts
			Scanner countfile = new Scanner(new File(cf));
			// Scanner for file containing gene lengths
			Scanner genefile = new Scanner(new File(gf));
			int genecount = -1;

			System.out.println("Open file " + cf);
			System.out.println("Open file " + gf);

			// Read the count file header
			String chead = countfile.nextLine();
			String[] tmp = chead.split("\t");
			// Read the gene length file header
			// genefile.nextLine();
			for (int s = 1; s < tmp.length; s++) {
				samplenames[s - 1] = tmp[s];
			}

			while (countfile.hasNextLine()) {
				genecount++;

				String cline = countfile.nextLine();
				String gline = genefile.nextLine();
				System.out.println(gline);

				String[] ctokens = cline.split("\t");
				String[] gtokens = gline.split("\t");

				String tmp_genename = ctokens[0];
				genenames[genecount] = gtokens[0];
				genelengths.add(Double.valueOf(gtokens[1]));

				if (!tmp_genename.equals(genenames[genecount])) {
					System.out
							.println("Validation error: genes in sample file not the same as genes in gene count file");
					System.out.println(tmp_genename + " != " + genenames[genecount]);
					System.exit(1);
				} else {
					for (int s = 1; s < ctokens.length; s++) {
						countmatrix[genecount][s - 1] = Integer.valueOf(ctokens[s]);
					}

				}
			}
			genefile.close();
			countfile.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		double threshold = 0.5;
		Options options = new Options();
		Option c = Option.builder("c").hasArg().required()
				.desc("countfile is a tab delimited file with the first column containing an identifier and the following"
						+ "  columns containing the counts per sample. The first line in the file should be a header line containing"
						+ "  the sample names. The first column of the header should contain a header for the id column.")
				.build();
		Option g = Option.builder("g").hasArg().required()
				.desc("genelengthsfile is a tab delimited file with two columns."
						+ " The first columns should contain the identifier"
						+ "  used in the counts file and the second column is the length of the gene in bases")
				.build();
		Option t = Option.builder("t").hasArg().required()
				.desc("tpmfilename is the name of the file to which the output will be written.").build();
		Option sn = Option.builder("sn").hasArg().required()
				.desc("number_of_samples is an integer indicating the number of samples "
						+ "thus equal to the number of columns in the countfile - 1.")
				.build();
		Option gn = Option.builder("gn").hasArg().required()
				.desc("give the number of genes in the file specified with the g switch.")
				.build();
		Option a = Option.builder("a").hasArg().required().desc("specify RPKM or TPM.").build();
		options.addOption(c);
		options.addOption(g);
		options.addOption(t);
		options.addOption(sn);
		options.addOption(gn);
		options.addOption(a);

		CommandLineParser parser = new DefaultParser();

		HelpFormatter formatter = new HelpFormatter();

		try {
			CommandLine cmd = parser.parse(options, args);
			if (cmd.hasOption("c"))
				cf = cmd.getOptionValue("c");
			if (cmd.hasOption("g"))
				gf = cmd.getOptionValue("g");
			if (cmd.hasOption("t"))
				tpmfilename = cmd.getOptionValue("t");
			if (cmd.hasOption("sn"))
				number_of_samples = Integer.valueOf(cmd.getOptionValue("sn"));
			if (cmd.hasOption("gn"))
				number_of_genes = Integer.valueOf(cmd.getOptionValue("gn"));
			if (cmd.hasOption("a"))
				argument = cmd.getOptionValue("a");

			try {

				File outfile = new File(tpmfilename);
				PrintWriter pw = new PrintWriter(outfile);
				readMatrixFile();
				ReadHTSeq rt = new ReadHTSeq(countmatrix, genelengths);
				double[][] tpms;
				if (argument.equals("TPM")) {
					System.out.println("Calculating TPMs ...");
					tpms = rt.TPM(countmatrix, genelengths);
				} else {
					System.out.println("Calculating RPKMs ...");
					tpms = rt.RPKM(countmatrix, genelengths);
				}
				pw.write("Gene_Name");
				for (int s = 0; s < number_of_samples; s++) {
					pw.write("\t" + samplenames[s]);
				}
				pw.write("\n");
				for (int f = 0; f < genenames.length; f++) {
					pw.write(genenames[f]);
					for (int s = 0; s < number_of_samples; s++) {
						pw.write("\t" + tpms[f][s]);
					}
					pw.write("\n");
				}

				pw.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (MissingOptionException e) {
			help(options);
		} catch (ParseException e) {
			System.err.println("Parsing failed.  Reason: " + e.getMessage());
		}

	}

	/**
	 * Prints the help text explaining command line parameters
	 * 
	 * @param options
	 */
	private static void help(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("java -cp MitoModel.jar com.jannetta.view.CalcMutPredLoad\n" + "Version: 1\n"
				+ "Version 1.0\n"
				+ "Calculate the MutPred load for all haplogrep files specified.", options);
	}
}
