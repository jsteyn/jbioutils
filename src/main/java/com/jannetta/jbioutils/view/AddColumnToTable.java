package com.jannetta.jbioutils.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Hashtable;
import java.util.Scanner;

/**
 * @author Jannetta S Steyn The purpose of this program is to read a list of of
 *         ids, with an associated attribute, from a tab delimited file e.g
 *         ENSCPO00000008243.1\tTMEM74. The associated attribute is then
 *         appended as a new column to a table. The table is provided separately
 *         as a tab delimited file.
 *
 */
public class AddColumnToTable {

	static int col;
	static String delim;
	static String idfile;
	static String tablefile;
	static String outfile;
	static Hashtable<String, String> id_att = new Hashtable<String, String>();

	public static void main(String[] args) {
		if (args.length < 5) {
			System.out.println("Sytax: java -jar AddColumnToTable.jar column(starting at 1) delim filewithids filewithtable outputfile");
		} else {
			try {
				col = Integer.valueOf(args[0]) - 1;
				if (args[1].equals("\\t")) {
					delim = "\t";
				} else
					delim = args[1];
				idfile = args[2];
				tablefile = args[3];
				outfile = args[4];
				System.out.println("Lookup file: " + idfile);
				System.out.println("Matrix file: " + tablefile);
				System.out.println("output: " + outfile);
				Scanner sc_idfile = new Scanner(new File(idfile));
				Scanner sc_tablefile = new Scanner(new File(tablefile));
				PrintWriter pw_outfile = new PrintWriter(new File(outfile));
				// Read id file into hashtable
				while (sc_idfile.hasNextLine()) {
					String line = sc_idfile.nextLine();
					String[] tokens = line.split("\t");
					id_att.put(tokens[0], tokens[1]);
				}

				while (sc_tablefile.hasNextLine()) {
					String line = sc_tablefile.nextLine();
					String[] tokens = line.split(delim);
					if (id_att.containsKey(tokens[col].trim())) {
						pw_outfile.println(line + delim + id_att.get(tokens[col]));
					} else {
						pw_outfile.println(line + "\tNA");
					}
				}
				pw_outfile.close();
				sc_tablefile.close();
				sc_idfile.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
