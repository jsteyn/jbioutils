package com.jannetta.jbioutils.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;

import com.jannetta.jbioutils.model.Genome;

public class GenomeStats {

	public static void main(String[] args) {
		Options options = new Options();
		CommandLineParser parser = new DefaultParser();
		String genomefile = "";

		Option g = Option.builder("g").hasArg().required()
				.desc("Name of genome file").build();
		options.addOption(g);
		try {
			CommandLine cmd = parser.parse(options, args);
			if (cmd.hasOption("g")) {
				genomefile = cmd.getOptionValue("g");
				countNucleotides(genomefile);
			}
		} catch (MissingOptionException e) {
			help(options);
			System.exit(1);
		} catch (UnrecognizedOptionException e) {
			help(options);
			System.exit(1);
		} catch (MissingArgumentException e) {
			System.out.println("You need to provide a value for -" + e.getOption().getOpt());
			System.exit(1);
		} catch (ParseException e) {
			System.err.println("Parsing failed.  Reason: " + e.getMessage());
			System.exit(1);
		}

	}

	private static void countNucleotides(String genomefile) {
		try {
			Scanner sc = new Scanner(new File(genomefile));
			Genome g = new Genome();
			// parse genome
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				if (line.startsWith(">")) {
					// do nothing
				} else {
					for (char c : line.toCharArray()) {
						switch (c) {
						case 'A':
							g.incA();
							break;
						case 'T':
							g.incT();
							break;
						case 'G':
							g.incG();
							break;
						case 'C':
							g.incC();
							break;
						case 'N':
							g.incN();
							break;
						}
					}
				}
			}
			System.out.println("all: " + g.total());
			System.out.println("N: " + g.getN());
			System.out.println("Perc N: " + g.getN()/(double)g.total()*100 + "%");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Prints the help text explaining command line parameters
	 * 
	 * @param options
	 */
	private static void help(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(
				"java -cp jbioutils.jar com.jannetta.view.GenomeStats\n" + "Version: 1\n"
						+ "Collating genome statistics",
				options);
	}
}
