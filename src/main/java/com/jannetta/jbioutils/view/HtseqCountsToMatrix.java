package com.jannetta.jbioutils.view;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.jannetta.jbioutils.controller.FileUtils;
import com.jannetta.jbioutils.controller.HtseqCountUtils;

public class HtseqCountsToMatrix {

	public static void main(String[] args) {
		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		String directory = "./";// directory where count files are saved
		String outputfile = "htseqcountmatrix.txt";

		Options options = new Options();
		Option d = Option.builder("d").hasArg()
				.desc("Location of the htseq-count files. If not specified it defaults to the current directoy")
				// .required()
				.build();
		Option o = Option.builder("o").hasArg()
				.desc("Output file. If not specified it will default to htseqcountmatrix.txt")
				// .required()
				.build();
		options.addOption(d);
		options.addOption(o);

		try {
			CommandLine cmd = parser.parse(options, args);
			if (cmd.hasOption("d")) {
				directory = cmd.getOptionValue("d");
			}
			if (cmd.hasOption("o")) {
				outputfile = cmd.getOptionValue("o");
			}
			File dir = new File(directory);
			if (dir.exists()) {
				System.out.println("Get count filenames in " + directory + "...");
				File[] files = new File(directory).listFiles();
				for (File f : files) {
					System.out.println(f.getPath());
				}
				HtseqCountUtils.createMatrixFile(files, outputfile);
			} else {
				System.out.println("The directory you specified does not exist.");
			}
		} catch (MissingOptionException e) {
			help(options);
		} catch (ParseException e) {
			System.err.println("Parsing failed.  Reason: " + e.getMessage());
		}

	}

	/**
	 * Prints the help text explaining command line parameters
	 * 
	 * @param options
	 */
	private static void help(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(
				"java -cp HtseqCountsToMatrix\n" + "Version: 1\n" + "Create a matrix of the htseq-count files.",
				options);
	}

}
